FROM node:20-alpine3.17

EXPOSE 3000

RUN mkdir -p /usr/app

COPY app /usr/app

WORKDIR /usr/app

RUN addgroup -S app && adduser -S app -G app

RUN chown -R app:app /usr/app

USER app

RUN npm install

CMD ["node", "server.js"]



